import os, pika
from flask import Flask, request, jsonify
#from configparser import SafeConfigParser
#from pathlib import Path

app = Flask(__name__)

#config_file = Path(os.environ.get('CONFIG_FILE','/etc/config'))
#parser = SafeConfigParser(os.environ)
#if config_file.is_file():
#    parser.read(os.environ.get('CONFIG_FILE'))

#host = parser.get('DEFAULT','RABBITMQ_HOST', fallback='localhost')
#port = parser.get('DEFAULT','RABBITMQ_PORT', fallback=5672) 
#queue = parser.get('DEFAULT','RABBITMQ_QUEUE', fallback='hello' )

#host = parser['DEFAULT']['RABBITMQ_HOST']
#port = parser['DEFAULT']['RABBITMQ_PORT']
#queue = parser['DEFAULT']['RABBITMQ_QUEUE']

host = os.getenv("RABBITMQ_HOST", "localhost")
port = os.getenv("RABBITMQ_PORT", 5672)
queue = os.getenv("RABBITMQ_QUEUE", "hello")

#bellow code is really bad.....
#host = Path('/etc/config/RABBITMQ_HOST').read_text()
#queue = Path('/etc/config/RABBITMQ_QUEUE').read_text()

html = """ 

<br>Escreva your favourite <i>pastel</i> flavour: 
<br>
<form method='POST' action='/'>
    <input type='text' name='flavour'>
    <input type='submit'>
</form>
"""


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        app.logger.info(request.form.get("flavour"))
        enqueue(request.form.get("flavour"))
    return html


@app.route('/health', methods=['GET'])
def health():
    return jsonify({"status": "ok"})


def enqueue(value):
    app.logger.info("Received message: %s", value)
    params = pika.ConnectionParameters(host=host, port=port)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='', routing_key=queue, body=value)
    connection.close()
    app.logger.info("Enqueued message on host %s:%s queue %s: %s", host, port,
                    queue, value)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8080)
